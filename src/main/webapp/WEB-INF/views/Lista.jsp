<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

<link rel="stylesheet" type="text/css" href="../style/main.css">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lista Estudiantes</title>
<%@ page isELIgnored="false" %>
</head>
<body>

<h1>LISTA</h1>
esta es la lista de los estudiantes

<h2>${message} </h2>

<table border="1">
	<c:forEach var="item" items="${lista}">
	
		<tr>
			<td>
				${item}
			</td>
			<td>
				10
			</td>
		</tr>
	</c:forEach>
</table>

</body>
</html>