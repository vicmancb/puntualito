package co.com.udem.puntu.controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/estudiantes")
public class EstudiantesController {
	
	@RequestMapping("/listaAPI")
	public @ResponseBody ArrayList<String> listaAPI(){
		ArrayList<String> estudiantes = new ArrayList<String>();
		
		estudiantes.add("Victor");
		estudiantes.add("Antonio");
		estudiantes.add("Andres");
		estudiantes.add("FErnando");
		
		return estudiantes;
	}
	
	@RequestMapping("/hello")
	public ModelAndView showMessage(
			@RequestParam(value = "name", required = false, defaultValue = "World") String name) {
		System.out.println("paramtro: " + name);
		
		ArrayList<String> estudiantes = new ArrayList<String>();
		
		estudiantes.add("Victor");
		estudiantes.add("Antonio");
		estudiantes.add("Andres");
		estudiantes.add("FErnando");
		
		ModelAndView mv = new ModelAndView("Lista");
		mv.addObject("message", "hola");
		mv.addObject("lista", estudiantes);
		return mv;
	}
}
